<?php
$cpt1 = 1;
$cpt2 = 1;
$cpt3 = 1;

// ------------ Fonctions --------------

function random_string(){
    $characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $r = rand(0, 25);
    $rchar = $characters[$r];
    return $rchar;
    }
function random_plus_moin(){
    $characters = "+-";
    $r = rand(0, 1);
    $rchar = $characters[$r];
    return $rchar;
    }
function random_solde(){
    $creer_solde = "";
    for ($i=0; $i < 5; $i++) { 
        if ($i == 0){
            $creer_solde[$i] = random_plus_moin();
        }
        elseif ($creer_solde[0] == "-" && $i == 2){
            $creer_solde[$i] = rand(1, 9);
        break;
        }
        else{
            $creer_solde[$i] = rand(1, 9);  
        }
    }
    return $creer_solde;
    }
function creer_agence($cpt1){
        $agence["nom_agence"] = readline("Veuillez entrer votre nom d'agence : ");
        $agence["adresse_banque"] = readline("Veuillez entrer l'adresse de votre agence : ");
        if ($cpt1 < 10){
            $agence["codeAgence"] = "00".$cpt1;
        }elseif ($cpt1 > 9 && $cpt1 < 100){
            $agence["codeAgence"] = "0".$cpt1;
        } else{
            $agence["codeAgence"] = $cpt1;
        }
        echo("votre agence ".$agence["nom_agence"] ." à pour code agence le numero " .$agence["codeAgence"] .PHP_EOL);
        return $agence;
    }
function creer_client($cpt2){
    if ($cpt2 < 10){
        $client["idClient"] = random_string() .random_string() ."00000".$cpt2;
    }elseif ($cpt2 > 9 && $cpt2 < 100){
        $client["idClient"] = "0000".$cpt2;
    }elseif ($cpt2 > 99 && $cpt2 < 1000){
        $client["idClient"] = "000".$cpt2;
    }elseif ($cpt2 > 999 && $cpt2 < 10000){
        $client["idClient"] = "00".$cpt2;
    }elseif ($cpt2 > 9999 && $cpt2 < 100000){
        $client["idClient"] = "0" .$cpt2;
    }else{
        $client["idClient"] = $cpt2;
    }
    $client["prenom"] = readline("Saisir le prénom du client : ");
    $client["nom"] = readline("Saisir le nom du client : ");
    $client["dateDeNaissance"] = readline("Saisir la date de naissance : ");
    $client["email"] = readline("Saisir l'email du client : ");
    echo("Client : " .$client["prenom"] ." " .$client["nom"] ." ID : " .$client["idClient"] ." crée");
    return $client;
    }
function creer_compte($cpt3,$agences,$clients){
    
    do{
    
    $creer_compte_idClient = readline ("Saisir l'identifiant du client à associer : ");

    // while (preg_match("#^[A-Z]{2}[0-9]{4}+$#", $creer_compte_idClient)){ //correction
    //     echo "---------> Votre saisie n'est pas valide".PHP_EOL;
    //     $creer_compte_idClient = readline ("Saisir l'identifiant du client à associer : ".PHP_EOL);
    // }
    $creer_compte_codeAgence = readline ("Saisir le code le l'agence à associer : ");

    // while (preg_match("#^[0-9]{3}$#",  $creer_compte_codeAgence)){ //correction
    //     echo "---------> Votre saisie n'est pas valide".PHP_EOL;
    //     $creer_compte_codeAgence = readline ("Saisir le code le l'agence à associer : ");
    // }
    $creer_compte_typeCompte = readline ("Choisir le type de compte (1 = Livret A, 2 = Compte courant, 3 = Plan épargne logement) : ");

    if ($creer_compte_typeCompte == 1){
        $creer_compte_typeCompte = "Livret A";
    }elseif ($creer_compte_typeCompte == 2){
        $creer_compte_typeCompte = "Compte courant";
    }elseif ($creer_compte_typeCompte == 3) {
        $creer_compte_typeCompte = "Plan épargne logement";
    } 
    else {
    echo "Saisie invalide"; // correction
    break;
    }
    $valid_id_code = true;//correction
    for ($i=0; $i < count($clients); $i++) {
        if ($creer_compte_idClient == $clients[$i]["idClient"] && $creer_compte_codeAgence == $agences[$i]["codeAgence"]){
            $valid_id_code = false;
            break;
        }
    }
    if ($valid_id_code){// correction
   
        echo("ID ou Code agence inconnu." .PHP_EOL);
        break;
    }
        $compte["idClient"] = $creer_compte_idClient;
        $compte["codeAgence"] = $creer_compte_codeAgence;
        $compte["typeCompte"] = $creer_compte_typeCompte;

        if ($cpt3 < 10){
            $compte["numéroDeCompte"] = "000000000".$cpt3;
        }elseif ($cpt3 > 9 && $cpt3 < 100){
            $compte["numéroDeCompte"] = "00000000".$cpt3;
        }elseif ($cpt3 > 99 && $cpt3 < 1000){
            $compte["numéroDeCompte"] = "000000".$cpt3;
        }elseif ($cpt3 > 999 && $cpt3 < 10000){
            $compte["numéroDeCompte"] = "00000".$cpt3;
        }elseif ($cpt3 > 99999 && $cpt3 < 1000000){
            $compte["numéroDeCompte"] = "0000".$cpt3;
        }elseif ($cpt3 > 999999 && $cpt3 < 10000000){
            $compte["numéroDeCompte"] = "000".$cpt3;
        }elseif ($cpt3 > 9999999 && $cpt3 < 100000000){
            $compte["numéroDeCompte"] = "00".$cpt3;
        }elseif ($cpt3 > 99999999 && $cpt3 < 1000000000){
            $compte["numéroDeCompte"] = "0".$cpt3;
        }elseif ($cpt3 > 999999999 && $cpt3 < 9999999999){
            $compte["numéroDeCompte"] = $cpt3;
        }

        //   --------- SOLDE ---------

        $compte["solde"] = random_solde();
        if($compte["solde"][0] == "-"){
            $compte["découvertAutorisé(O/N)"] = "O";
        }else{
            $x = rand(0,1);
        }

        if (isset ($x) && $x == 1){ //correction
             $compte["découvertAutorisé(O/N)"] = "N";
        }
        else{
            $compte["découvertAutorisé(O/N)"] = "O";  
        }
    
        echo ("Compte n° ".$compte["numéroDeCompte"] ." du client ".$compte["idClient"] ." crée");
        return $compte;

    break;
    
    
    }while(true);
}
function creer_compte_sans_doublon_type($cpt3,$agences,$clients,$comptes){

    do{
        $creer_compte_idClient = readline ("Saisir l'identifiant du client à associer : ");
        
        while (preg_match("#^[A-Z]{2}[0-9]{6}+$#", $creer_compte_idClient)){ //correction
            echo "---------> Votre saisie n'est pas valide".PHP_EOL;
            $creer_compte_idClient = readline ("Saisir l'identifiant du client à associer : ".PHP_EOL);
        } 

        $creer_compte_codeAgence = readline ("Saisir le code le l'agence à associer : ");
        while (preg_match("#^[0-9]+{3}$#",  $creer_compte_codeAgence)){ //correction
            echo "---------> Votre saisie n'est pas valide".PHP_EOL;
            $creer_compte_codeAgence = readline ("Saisir le code le l'agence à associer : ");
        }

        $creer_compte_typeCompte = readline ("Choisir le type de compte (1 = Livret A, 2 = Compte courant, 3 = Plan épargne logement) : ");
        
        if (preg_match("#^[123]$#", $creer_compte_typeCompte)) { //correction
            if ($creer_compte_typeCompte == 1){
                $creer_compte_typeCompte = "Livret A";
            }elseif ($creer_compte_typeCompte == 2){
                $creer_compte_typeCompte = "Compte courant";
            }elseif ($creer_compte_typeCompte == 3) {
                $creer_compte_typeCompte = "Plan épargne logement";
            } 
            else {
                echo "Saisie invalide"; // correction
                break;
                }
            $valid_id_code = true; //correction
            for ($i=0; $i < count($clients); $i++) {
                if ($creer_compte_idClient != $clients[$i]["idClient"] && $creer_compte_codeAgence != $agences[$i]["codeAgence"]){// correction
                    $valid_id_code = false;
                    echo("ID ou Code agence inconnu." .PHP_EOL);
                    break;
                }
            }
            $valid_type= true;// correction
            for ($i=0; $i < count($comptes); $i++) {
                if ($creer_compte_typeCompte == $comptes[$i]["typeCompte"]){ //a verifier
                    echo("Impossible de créer deux comptes du même type." .PHP_EOL);
                    $valid_type= false;//correction
                    break; // correction
                }
            }
            
                if ($valid_id_code && $valid_type) {//correction


                    $compte["idClient"] = $creer_compte_idClient;
                    $compte["codeAgence"] = $creer_compte_codeAgence;
                    $compte["typeCompte"] = $creer_compte_typeCompte;

                    if ($cpt3 < 10){
                            $compte["numéroDeCompte"] = "000000000".$cpt3;
                    }elseif ($cpt3 > 9 && $cpt3 < 100){
                            $compte["numéroDeCompte"] = "00000000".$cpt3;
                    }elseif ($cpt3 > 99 && $cpt3 < 1000){
                            $compte["numéroDeCompte"] = "000000".$cpt3;
                    }elseif ($cpt3 > 999 && $cpt3 < 10000){
                            $compte["numéroDeCompte"] = "00000".$cpt3;
                    }elseif ($cpt3 > 99999 && $cpt3 < 1000000){
                            $compte["numéroDeCompte"] = "0000".$cpt3;
                    }elseif ($cpt3 > 999999 && $cpt3 < 10000000){
                            $compte["numéroDeCompte"] = "000".$cpt3;
                    }elseif ($cpt3 > 9999999 && $cpt3 < 100000000){
                            $compte["numéroDeCompte"] = "00".$cpt3;
                    }elseif ($cpt3 > 99999999 && $cpt3 < 1000000000){
                            $compte["numéroDeCompte"] = "0".$cpt3;
                    }elseif ($cpt3 > 999999999 && $cpt3 < 9999999999){
                            $compte["numéroDeCompte"] = $cpt3;
                    }
                
                    //   --------- SOLDE ---------

                    $compte["solde"] = random_solde();
                    
                    if($compte["solde"][0] == "-"){
                        $compte["découvertAutorisé(O/N)"] = "O";
                    }else{
                        $x = rand(0,1);
                    }
                    if (isset($x) && $x == 1){ //correction
                        $compte["découvertAutorisé(O/N)"] = "N";
                    }
                    else{
                        $compte["découvertAutorisé(O/N)"] = "O";  
                    }
                
                    echo ("Compte n° ".$compte["numéroDeCompte"] ." du client ".$compte["idClient"] ." crée");
                    return $compte;
                
                break;
                }
        }
        else {
            echo "Saisie invalide"; // correction
            break;
            }
        
    }while(true);
    }
function cherche_compte($comptes){
    $numero_compte_recherche = readline("Veuillez entrer le numéro de compte à rechercher : ");
    for ($i = 0; $i < count($comptes) ; $i++) {
        if ($comptes[$i]["numéroDeCompte"] == $numero_compte_recherche ) {
            echo (" - Informations du compte : ");
            echo ($comptes[$i]["numéroDeCompte"].PHP_EOL);
            echo (", Type de compte : ");
            echo ($comptes[$i]["typeCompte"].PHP_EOL);
            echo (", Solde du compte : ");
            echo ($comptes[$i]["solde"].PHP_EOL);
            echo (", Découvert autorisé : ");
            echo ($comptes[$i]["découvertAutorisé(O/N)"].PHP_EOL);
            echo (", ID du propriétaire : ");
            echo ($comptes[$i]["idClient"].PHP_EOL);
            echo (", Code de l'agence associée : ");
            echo ($comptes[$i]["codeAgence"] .PHP_EOL);
        }
    }
    }
function cherche_client($comptes,$clients){
    while ($menu != '4') {
           
        echo('1 - Recherche par id client'.PHP_EOL);
        echo('2 - Recherche par numero de compte'.PHP_EOL);
        echo('3 - Recherche par nom du client'.PHP_EOL);  
        echo('4 - Revenir au menu principal ');
        
        $menu = readline("Veuillez choisir entre 1 et 4 : ");  
            
        if ($menu == '1') {
           
            for ($i=0; $i < count($comptes); $i++) { 
            $idClient = readline("Saisir l'identifiant du client recherché : ");
            
                if ($idClient == $comptes[$i]["idClient"]){
                    echo (" - Informations du client : ");
                    echo ($clients[$i]["prenom"]);
                    echo (" ");
                    echo ($clients[$i]["nom"]);
                    echo (" : ID : ");
                    echo ($clients[$i]["idClient"]);
                    echo (", Né(e) le : ");
                    echo ($clients[$i]["dateDeNaissance"]);
                    echo (", Coordonnées : ");
                    echo ($clients[$i]["email"] .PHP_EOL);
                    $check1 = true;
                break;
                }
            }
        }
        elseif ($menu == '2') {
            for ($i=0; $i < count($comptes); $i++) { 
            $numCompte = readline("Saisir le numéro du compte du client recherché : ". PHP_EOL);
                if ($numCompte == $comptes[$i]["numéroDeCompte"]){
                    echo (" - Informations du client : ");
                    echo ($clients[$i]["prenom"]);
                    echo (" ");
                    echo ($clients[$i]["nom"]);
                    echo (" : ID : ");
                    echo ($clients[$i]["idClient"]);
                    echo (", Né(e) le : ");
                    echo ($clients[$i]["dateDeNaissance"]);
                    echo (", Coordonnées : ");
                    echo ($clients[$i]["email"] .PHP_EOL);
                    
                break;
                }
            }
        }
        elseif ($menu == '3') {
        
            for ($i=0; $i < count($clients); $i++) {
                 $nom = readline("Saisir le nom du client recherché : ". PHP_EOL);
                if ($nom == $clients[$i]["nom"]){
                    echo (" - Informations du client : ");
                    echo ($clients[$i]["prenom"]);
                    echo (" ");
                    echo ($clients[$i]["nom"]);
                    echo (" : ID : ");
                    echo ($clients[$i]["idClient"]);
                    echo (", Né(e) le : ");
                    echo ($clients[$i]["dateDeNaissance"]);
                    echo (", Coordonnées : ");
                    echo ($clients[$i]["email"] .PHP_EOL);
        
                    
                break;
                }
            }
        }elseif ($menu == '4') {
        break;
         }
        
        
             else{
                echo("Veuillez saisir des informations valides ou créer le client ou le compte bancaire" .PHP_EOL);
                $menu = readline("Veuillez choisir entre 1 et 4 : ");
        
            }
    }
    }
function afficher_tout_agence($agences){
    if (empty($agences)){
        echo ("Aucune agence.".PHP_EOL);
    } else{
        for ($i = 0; $i < count($agences); $i++){
            echo (" - Informations de l'agence : ");
            echo ($agences[$i]["nom_agence"]);
            echo (", Code agence : ");
            echo ($agences[$i]["codeAgence"]);
            echo (", Coordonnées : ");
            echo ($agences[$i]["adresse_banque"] .PHP_EOL);
        }
    }
    }
function afficher_tout_client($clients){
    if (empty($clients)){
        echo ("Aucun client".PHP_EOL);
    } else{
        for ($i = 0;$i < count($clients); $i++){
            echo (" - Informations du client : ");
            echo ($clients[$i]["prenom"]);
            echo (" ");
            echo ($clients[$i]["nom"]);
            echo (" : ID : ");
            echo ($clients[$i]["idClient"]);
            echo (", Né(e) le : ");
            echo ($clients[$i]["dateDeNaissance"]);
            echo (", Coordonnées : ");
            echo ($clients[$i]["email"] .PHP_EOL);
        }
    }
    }
function afficher_tout_compte($comptes){
    if (empty($comptes)){
        echo ("Aucun compte".PHP_EOL);
    } else{
        for ($i = 0;$i < count($comptes); $i++){
            echo (" - Informations du compte : ");
            echo ($comptes[$i]["numéroDeCompte"]);
            echo (", Type de compte : ");
            echo ($comptes[$i]["typeCompte"]);
            echo (", Solde du compte : ");
            echo ($comptes[$i]["solde"]);
            echo (", Découvert autorisé : ");
            echo ($comptes[$i]["découvertAutorisé(O/N)"]);
            echo (", ID du propriétaire : ");
            echo ($comptes[$i]["idClient"]);
            echo (", Code de l'agence associée : ");
            echo ($comptes[$i]["codeAgence"] .PHP_EOL);
        }
    }
    }
function afficher_compte_client($comptes){
    $identifiant_client_recherche = readline ("Veuillez entrer l'identifiant du client : ");
        for ($i = 0; $i < count($comptes); $i++) {
            if ($comptes[$i]["idClient"] == $identifiant_client_recherche) {
                echo (" - Informations du compte : ");
                echo ($comptes[$i]["numéroDeCompte"].PHP_EOL);
                echo (", Type de compte : ");
                echo ($comptes[$i]["typeCompte"].PHP_EOL);
                echo (", Solde du compte : ");
                echo ($comptes[$i]["solde"].PHP_EOL);
                echo (", Découvert autorisé : ");
                echo ($comptes[$i]["découvertAutorisé(O/N)"].PHP_EOL);
                echo (", ID du propriétaire : ");
                echo ($comptes[$i]["idClient"].PHP_EOL);
                echo (", Code de l'agence associée : ");
                echo ($comptes[$i]["codeAgence"] .PHP_EOL);
            }
        }
    }
function impr_info_client($comptes,$clients){

    $idClient = readline("Saisir l'identifiant du client recherché : ");
    $nom = readline("Saisir le nom du client recherché : ");

    for ($i=0; $i < count($comptes); $i++) { 
    
        if ($idClient == $comptes[$i]["idClient"]){
            $check1 = true;
        break;
        }
    }
    for ($i=0; $i < count($clients); $i++) { 
        if ($nom == $clients[$i]["nom"]){
            $check2 = true;
        break;
        }
    }
    if ($check1 && $check2){
        for ($i=0; $i < count($clients); $i++) { 
            if ($nom == $clients[$i]["nom"]){
                $my_file = ("fiche_" .$clients[$i]["nom"] ."_" .$clients[$i]["prenom"] .".txt");
                $handle = fopen($my_file, "w") or die("Cannot open file:  ".$my_file);
                $data[] = "--------------------------------------------------------------------------------------------------" ."\n"."\n";
                $data[] = "ID client : " .$clients[$i]["idClient"] ."\n"."\n";
                $data[] = "Nom : ".$clients[$i]["nom"] ."\n"."\n";
                $data[] = "Prénom : ".$clients[$i]["prenom"] ."\n"."\n";
                $data[] = "Date de naissance : ".$clients[$i]["dateDeNaissance"] ."\n"."\n";
                $data[] = "--------------------------------------------------------------------------------------------------" ."\n"."\n";
                $data[] = "Liste des comptes" ."\n"."\n";
                $data[] = "Numéros de compte                     Soldes" ."\n"."\n";
            break;
            } else{
                echo("Veuillez saisir des informations valides ou enregistrez le client ou le compte bancaire" .PHP_EOL);
            }
        }   
        $data_plus = [];
        $x = 0;
        for ($i=0; $i < count($comptes); $i++) { 
            if ($idClient == $comptes[$i]["idClient"]){
                $data_plus[] = "".$comptes[$i]["numéroDeCompte"]."                          ".$comptes[$i]["solde"] ."€ \n";
                if($comptes[$i]["solde"][0] == "-"){
                    $data_plus[] = ":-(" ."\n"."\n";
                } else {
                    $data_plus[] = ":-)" ."\n"."\n";
                }
            $x += 1;
            }
        }
        $data_plus[] = "--------------------------------------------------------------------------------------------------";
        fwrite($handle, implode($data));
        fwrite($handle, implode($data_plus));
        echo ("Fiche client créee.");
    }
}
function ajout_csv_tout($agences,$clients,$comptes){
    $fp = fopen("file.csv", "w");
    foreach ($agences as $val) {
        fputcsv($fp, $val);
    }
    foreach ($clients as $val) {
        fputcsv($fp, $val);
    }
    foreach ($comptes as $val) {
        fputcsv($fp, $val);
    }
    fclose($fp);
    }
function ajout_csv_agence($agences){
    $fp = fopen("file.csv", "w");
    foreach ($agences as $val) {
        fputcsv($fp, $val);
    }
    fclose($fp);
    }
function ajout_csv_client($clients){
    $fp = fopen("file.csv", "w");
    foreach ($clients as $val) {
        fputcsv($fp, $val);
    }
    fclose($fp);
    }
// ------------ Menu --------------
do {

    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);
    echo (" ---------- Menu de la Banque ---------- ");
    echo (" " .PHP_EOL);
    echo (" " .PHP_EOL);

    echo ("(1) Créer une agence" .PHP_EOL);
    echo ("(2) Créer un client" .PHP_EOL);
    echo ("(3) Créer un compte bancaire" .PHP_EOL);
	echo ("(4) Recherche de compte (numéro de compte)" .PHP_EOL);
	echo ("(5) Recherche de client (Nom, Numéro de compte, identifiant de client)" .PHP_EOL);
	echo ("(6) Afficher la liste des comptes d’un client (identifiant client)" .PHP_EOL);
	echo ("(7) Imprimer les infos client (identifient client)" .PHP_EOL);	
    echo ("(8) Afficher toutes les agences" .PHP_EOL);
    echo ("(9) Afficher tout les clients" .PHP_EOL);
    echo ("(10) Afficher tout les comptes bancaires" .PHP_EOL);
    echo ("(11) Stockage csv" .PHP_EOL);
    echo ("(12) Quitter le programme" .PHP_EOL);

    echo (" " .PHP_EOL);

    $choix_menu = readline("Choisir une des fonctionalités du menu : ");

    switch ($choix_menu) {
        case 1:
                $agences[] = creer_agence($cpt1++);
            break;
        case 2:
                $clients[] = creer_client($cpt2++);
            break;
        case 3:
            if (empty($clients) || empty($agences)){ //correction
                echo ("Pas de client ou d'agence enregistré." .PHP_EOL);
            }elseif(empty($comptes)){
                $comptes[] = creer_compte($cpt3++,$agences,$clients);
            }else{
                $comptes[] = creer_compte_sans_doublon_type($cpt3++,$agences,$clients,$comptes);
            }
            break;   
        case 4:
            if (empty($comptes)){
                echo("Pas de compte enregisté." .PHP_EOL);
            }else{
            cherche_compte($comptes);
            }
            break;
        case 5:
            cherche_client($comptes,$clients);
            break;
        case 6:
            afficher_compte_client($comptes);
            break;
        case 7:
            impr_info_client($comptes,$clients);
            break;
        case 8:
            afficher_tout_agence($agences);
            break;
        case 9:
            afficher_tout_client($clients);
            break;
        case 10:
            afficher_tout_compte($comptes);  
            break;
        case 11:
            if(empty($comptes) && empty($clients)) {
                ajout_csv_agence($agences);
            }
            if(empty($comptes) && empty($agences)){
                ajout_csv_client($clients);
            }else{
                ajout_csv_tout($clients,$comptes,$agences);
            }
            break;
    }
    if ($choix_menu == 12){
    break;
    }
} while (true);
?>