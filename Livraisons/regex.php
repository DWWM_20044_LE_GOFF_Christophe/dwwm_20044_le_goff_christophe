<?php

// ls toto.php -l tata titi -i -rs
$ligne_commande_globale = readline("chemin> ");

$commandes = [];
$estValide = preg_match("#(^[a-z]+)[ ]+([a-z]+\.[a-z]+)[ ]+(\-[a-z]+)*[ ]*([a-z]*)[ ]*([a-z]*)[ ]*(\-[a-z]+)*[ ]*(\-[a-z]+)*#", $ligne_commande_globale, $commandes);

echo (PHP_EOL);
$commande = $commandes[1];
echo ("la commande est " . $commande . PHP_EOL . PHP_EOL);

foreach ($commandes as $key => $elements) {
    if (strstr($elements, "-") && $key != 0) {
        if (strlen($elements) > 2) {
            $new_option = str_split(strstr($elements, "-"));
            for ($i = 1; $i < count($new_option); $i++) {
                $commande_options[] = $new_option[$i];
            }
        } elseif (strlen($elements) == 2)
            $commande_options[] = substr($elements, 1);
    } elseif ($key > 1) {
        $commande_args[] = $elements;
    }
}

echo ("les arguments en entrée  : " . PHP_EOL);
foreach ($commande_args as $arg) {
    echo ($arg . ", ");
}
echo (PHP_EOL . PHP_EOL);

echo ("Options : " . PHP_EOL);
foreach ($commande_options as $option) {
    echo ($option . ", ");
}

echo (PHP_EOL . PHP_EOL);
